package com.haiduc.alina.sorting.methods;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import java.io.File;
import java.util.Arrays;
import java.util.Random;

public class ComparisonAverage {
    public static void main(String[] args) {

        int dimensionStart = 100, dimensionStop = 10_000, increment = 100, m = 10;
        int numberArrays = 0;
        double sumAssignmentsBubble, sumComparisonsBubble, sumOperationsBubble;
        double sumAssignmentsMerge, sumComparisonsMerge, sumOperationsMerge;
        double sumAssignmentsQuick, sumComparisonsQuick, sumOperationsQuick;
        double averageAssignmentsBubble, averageComparisonsBubble, averageOperationsBubble;
        double averageAssignmentsMerge, averageComparisonsMerge, averageOperationsMerge;
        double averageAssignmentsQuick, averageComparisonsQuick, averageOperationsQuick;

        ExcelMethods.createExcel("Average case scenario");
        ExcelMethods.iterationNumber("Average case scenario", dimensionStop, increment);

        // Generate random array of different dimensions
        Random random = new Random();
        for (int arrayDim = dimensionStart; arrayDim <= dimensionStop; arrayDim += increment) {
            numberArrays++;
            int[] generatedArray = new int[arrayDim];
            sumAssignmentsBubble = 0;
            sumComparisonsBubble = 0;
            sumOperationsBubble = 0;
            sumAssignmentsMerge = 0;
            sumComparisonsMerge = 0;
            sumOperationsMerge = 0;
            sumAssignmentsQuick = 0;
            sumComparisonsQuick = 0;
            sumOperationsQuick = 0;
            for (int h = 1; h <= m; h++) {
                for (int i = 0; i < generatedArray.length; i++) {
                    generatedArray[i] = random.nextInt(dimensionStop);
                }
                System.out.println("Original generated array: " + Arrays.toString(generatedArray));

                int[] bubbleArray = new int[generatedArray.length];
                for (int i = 0; i < generatedArray.length; i++) {
                    bubbleArray[i] = generatedArray[i];
                }

                int[] mergeArray = new int[generatedArray.length];
                for (int i = 0; i < generatedArray.length; i++) {
                    mergeArray[i] = generatedArray[i];
                }

                int[] quickArray = new int[generatedArray.length];
                for (int i = 0; i < generatedArray.length; i++) {
                    quickArray[i] = generatedArray[i];
                }

                // Bubble sort
                //System.out.println("Input array for BubbleSort: " + Arrays.toString(bubbleArray));
                int[] resultBubble = BubbleSortAlgorithm.bubbleSort(bubbleArray);
                sumAssignmentsBubble = sumAssignmentsBubble + resultBubble[0];
                sumComparisonsBubble = sumComparisonsBubble + resultBubble[1];
                sumOperationsBubble = sumOperationsBubble + resultBubble[2];

                // Merge sort
                //System.out.println("Input array for MergeSort: " + Arrays.toString(mergeArray));
                MergeSortAlgorithm obM = new MergeSortAlgorithm();
                int[] resultMerge = obM.mergeSort(mergeArray, mergeArray.length);
                //System.out.println("Array after MergeSort: " + Arrays.toString(mergeArray));
                //System.out.println("Assignments MergeSort: " + resultMerge[0]);
                //System.out.println("Comparisons MergeSort: " + resultMerge[1]);
                //System.out.println("Comparisons MergeSort: " + resultMerge[2]);
                sumAssignmentsMerge = sumAssignmentsMerge + resultMerge[0];
                sumComparisonsMerge = sumComparisonsMerge + resultMerge[1];
                sumOperationsMerge = sumOperationsMerge + resultMerge[2];

                // Quick sort
                //System.out.println("Input array for QuickSort: " + Arrays.toString(quickArray));
                QuickSortAlgorithm obQ = new QuickSortAlgorithm();
                int[] resultQuick = obQ.quickSort(quickArray, 0, quickArray.length - 1);
                //System.out.println("Array after QuickSort: " + Arrays.toString(quickArray));
                //System.out.println("Assignments QuickSort: " + resultQuick[0]);
                //System.out.println("Comparisons QuickSort: " + resultQuick[1]);
                //System.out.println("Operations QuickSort: " + resultQuick[2]);
                sumAssignmentsQuick = sumAssignmentsQuick + resultQuick[0];
                sumComparisonsQuick = sumComparisonsQuick + resultQuick[1];
                sumOperationsQuick = sumOperationsQuick + resultQuick[2];

            }
            averageAssignmentsBubble = sumAssignmentsBubble / m;
            averageComparisonsBubble = sumComparisonsBubble / m;
            averageOperationsBubble = sumOperationsBubble / m;
            //System.out.println("AverageAssignmentsBubble: " + averageAssignmentsBubble);
            //System.out.println("AverageComparisonsBubble: " + averageComparisonsBubble);
            //System.out.println("AverageOperationsBubble: " + averageOperationsBubble);

            averageAssignmentsMerge = sumAssignmentsMerge / m;
            averageComparisonsMerge = sumComparisonsMerge / m;
            averageOperationsMerge = sumOperationsMerge / m;
            //System.out.println("AverageAssignmentsMerge: " + averageAssignmentsMerge);
            //System.out.println("AverageComparisonsMerge: " + averageComparisonsMerge);
            //System.out.println("AverageOperationsMerge: " + averageOperationsMerge);

            averageAssignmentsQuick = sumAssignmentsQuick / m;
            averageComparisonsQuick = sumComparisonsQuick / m;
            averageOperationsQuick = sumOperationsQuick / m;
            //System.out.println("AverageAssignmentsQuick: " + averageAssignmentsQuick);
            //System.out.println("AverageComparisonsQuick: " + averageComparisonsQuick);
            //System.out.println("AverageOperationsQuick: " + averageOperationsQuick);

            try {
                Workbook wb = Workbook.getWorkbook(new File("Average case scenario.xls"));
                WritableWorkbook copy = Workbook.createWorkbook(new File("Average case scenario.xls"), wb);
                WritableSheet copySheet = copy.getSheet(0);
                Label label1 = new Label(1, numberArrays, String.valueOf(arrayDim));
                copySheet.addCell(label1);

                Label label2 = new Label(2, numberArrays, String.valueOf(averageAssignmentsBubble));
                copySheet.addCell(label2);
                Label label3 = new Label(3, numberArrays, String.valueOf(averageComparisonsBubble));
                copySheet.addCell(label3);
                Label label4 = new Label(4, numberArrays, String.valueOf(averageOperationsBubble));
                copySheet.addCell(label4);

                Label label5 = new Label(5, numberArrays, String.valueOf(averageAssignmentsMerge));
                copySheet.addCell(label5);
                Label label6 = new Label(6, numberArrays, String.valueOf(averageComparisonsMerge));
                copySheet.addCell(label6);
                Label label7 = new Label(7, numberArrays, String.valueOf(averageOperationsMerge));
                copySheet.addCell(label7);

                Label label8 = new Label(8, numberArrays, String.valueOf(averageAssignmentsQuick));
                copySheet.addCell(label8);
                Label label9 = new Label(9, numberArrays, String.valueOf(averageComparisonsQuick));
                copySheet.addCell(label9);
                Label label10 = new Label(10, numberArrays, String.valueOf(averageOperationsQuick));
                copySheet.addCell(label10);

                copy.write();
                copy.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}


