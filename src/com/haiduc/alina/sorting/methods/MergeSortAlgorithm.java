package com.haiduc.alina.sorting.methods;

public class MergeSortAlgorithm {

    int noAssignmentsMerge = 0;
    int noComparisonsMerge = 0;

    public void merge(int[] array, int[] leftArray, int[] rightArray, int left, int right) {
        noAssignmentsMerge++;
        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            noComparisonsMerge++;
            noComparisonsMerge++;
            noComparisonsMerge++;
            if (leftArray[i] <= rightArray[j]) {
                array[k++] = leftArray[i++];
                noAssignmentsMerge++;
            } else {
                array[k++] = rightArray[j++];
                noAssignmentsMerge++;
            }
        }
        noComparisonsMerge++;
        while (i < left) {
            array[k++] = leftArray[i++];
            noAssignmentsMerge++;
        }
        noComparisonsMerge++;
        while (j < right) {
            array[k++] = rightArray[j++];
            noAssignmentsMerge++;
        }
        return;
    }

    // Main function that sorts arr[0...n] using merge()
    public int[] mergeSort(int[] array, int n) {
        noAssignmentsMerge++;
        int[] result = new int[3];
        noComparisonsMerge++;
        if (n < 2) {
            noAssignmentsMerge++;
            int noOperationsMerge = noAssignmentsMerge + noComparisonsMerge;
            result[0] = noAssignmentsMerge;
            result[1] = noComparisonsMerge;
            result[2] = noOperationsMerge;
            return result;
        } else {
            int mid = n / 2;
            int[] leftArray = new int[mid];
            int[] rightArray = new int[n - mid];

            for (int i = 0; i < mid; i++) {
                noAssignmentsMerge++;
                leftArray[i] = array[i];
            }
            for (int i = mid; i < n; i++) {
                noAssignmentsMerge++;
                rightArray[i - mid] = array[i];
            }
            mergeSort(leftArray, mid);
            mergeSort(rightArray, n - mid);

            merge(array, leftArray, rightArray, mid, n - mid);
        }
        int noOperationsMerge = noAssignmentsMerge + noComparisonsMerge;
        result[0] = noAssignmentsMerge;
        result[1] = noComparisonsMerge;
        result[2] = noOperationsMerge;
        return result;
    }
}
