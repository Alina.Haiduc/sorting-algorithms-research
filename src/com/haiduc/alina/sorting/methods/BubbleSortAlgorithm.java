package com.haiduc.alina.sorting.methods;

import java.util.Arrays;

public class BubbleSortAlgorithm {
    static int[] bubbleSort(int[] array) {
        int[] result = new int[3];
        int i, j, temp;
        int noAssignmentsBubble = 0;
        int noComparisonsBubble = 0;
        boolean swapped;
        for (i = 0; i < array.length - 1; i++) {
            swapped = false;
            noAssignmentsBubble++;
            for (j = 0; j < array.length - i - 1; j++) {
                noAssignmentsBubble++;
                noComparisonsBubble++;
                if (array[j] > array[j + 1]) {
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                    swapped = true;
                }
            }
            if (swapped == false)
                break;
        }
        int noOperationsBubble = noAssignmentsBubble + noComparisonsBubble;

        //System.out.println("Array after BubbleSort: " + Arrays.toString(array));
        //System.out.println("Assignments BubbleSort = " + noAssignmentsBubble);
        //System.out.println("Comparisons BubbleSort = " + noComparisonsBubble);
        //System.out.println("Operations BubbleSort = " + noOperationsBubble);

        result[0]=noAssignmentsBubble;
        result[1]=noComparisonsBubble;
        result[2]=noOperationsBubble;

        return result;
    }
}

