package com.haiduc.alina.sorting.methods;

public class QuickSortAlgorithm {

    int noAssignmentsQuick = 0;
    int noComparisonsQuick = 0;

    public int partition(int array[], int low, int high) {
        int pivot = array[high];
        int i = (low - 1); // index of smaller element
        for (int j = low; j < high; j++) {
            // If current element is smaller than the pivot
            noAssignmentsQuick++;
            noComparisonsQuick++;
            if (array[j] < pivot) {
                i++;

                // swap arr[i] and arr[j]
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
        // swap arr[i+1] and arr[high] (or pivot)
        noAssignmentsQuick++;
        int temp = array[i + 1];
        array[i + 1] = array[high];
        array[high] = temp;
        return i + 1;
    }

    //The main function that implements QuickSort(); arr[] --> Array to be sorted, low  --> Starting index, high  --> Ending index
    public int[] quickSort(int array[], int low, int high) {
        int[] result = new int[3];
        if (low < high) {
            //pi is partitioning index, arr[pi] is now at right place
            int pi = partition(array, low, high);

            // Recursively sort elements before
            // partition and after partition
            quickSort(array, low, pi - 1);
            quickSort(array, pi + 1, high);

        }
        int noOperationsQuick = noAssignmentsQuick + noComparisonsQuick;
        result[0] = noAssignmentsQuick;
        result[1] = noComparisonsQuick;
        result[2] = noOperationsQuick;
        return result;
    }
}
