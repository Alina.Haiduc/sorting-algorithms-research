package com.haiduc.alina.sorting.methods;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import java.io.File;

public class ExcelMethods {
    public static void createExcel(String name) {
        try {
            WritableWorkbook workbook = Workbook.createWorkbook(new File(name + ".xls"));
            workbook.createSheet("Sheet1", 0);
            Label label1 = new Label(0, 0, "Nr.Crt.");
            Label label2 = new Label(1, 0, "Number of elements in array");
            Label label3 = new Label(2, 0, "Number of assignments Bubble Sort");
            Label label4 = new Label(3, 0, "Number of comparisons Bubble Sort");
            Label label5 = new Label(4, 0, "Number of operations Bubble Sort");
            Label label6 = new Label(5, 0, "Number of assignments Merge Sort");
            Label label7 = new Label(6, 0, "Number of comparisons Merge Sort");
            Label label8 = new Label(7, 0, "Number of operations Merge Sort");
            Label label9 = new Label(8, 0, "Number of assignments Quick Sort");
            Label label10 = new Label(9, 0, "Number of comparisons Quick Sort");
            Label label11 = new Label(10, 0, "Number of operations Quick Sort");
            WritableSheet copySheet = workbook.getSheet(0);
            copySheet.addCell(label1);
            copySheet.addCell(label2);
            copySheet.addCell(label3);
            copySheet.addCell(label4);
            copySheet.addCell(label5);
            copySheet.addCell(label6);
            copySheet.addCell(label7);
            copySheet.addCell(label8);
            copySheet.addCell(label9);
            copySheet.addCell(label10);
            copySheet.addCell(label11);
            workbook.write();
            workbook.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void iterationNumber(String name, int n, int s) {
        for (int i = 1; i <= n / s; i++) {
            try {
                Workbook wb = Workbook.getWorkbook(new File(name + ".xls"));
                WritableWorkbook copy = Workbook.createWorkbook(new File(name + ".xls"), wb);
                WritableSheet copySheet = copy.getSheet(0);
                Label label = new Label(0, i, String.valueOf(i));
                copySheet.addCell(label);
                copy.write();
                copy.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}