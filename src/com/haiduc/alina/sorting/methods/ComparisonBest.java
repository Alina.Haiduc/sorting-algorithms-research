package com.haiduc.alina.sorting.methods;

import java.io.File;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import java.util.Arrays;
import java.util.Random;

public class ComparisonBest {
    public static void main(String[] args) {

        int dimensionStart = 100, dimensionStop = 10_000, increment = 100;
        int numberArrays = 0;

        ExcelMethods.createExcel("Best case scenario");
        ExcelMethods.iterationNumber("Best case scenario", dimensionStop, increment);

        Random random = new Random();
        for (int arrayDim = dimensionStart; arrayDim <= dimensionStop; arrayDim += increment) {
            numberArrays++;
            int[] generatedArray = new int[arrayDim];
            for (int i = 0; i < generatedArray.length; i++) {
                generatedArray[i] = random.nextInt(dimensionStop);
            }
            System.out.println("Original generated array: " + Arrays.toString(generatedArray));
            Arrays.sort(generatedArray);
            System.out.println("Original array sorted for best case scenario: " + Arrays.toString(generatedArray));

            int[] bubbleArray = new int[generatedArray.length];
            for (int i = 0; i < generatedArray.length; i++) {
                bubbleArray[i] = generatedArray[i];
            }

            int[] mergeArray = new int[generatedArray.length];
            for (int i = 0; i < generatedArray.length; i++) {
                mergeArray[i] = generatedArray[i];
            }

            int[] quickArray = new int[generatedArray.length];
            for (int i = 0; i < generatedArray.length; i++) {
                quickArray[i] = generatedArray[i];
            }

            // Bubble sort
            //System.out.println("Input array for BubbleSort: " + Arrays.toString(bubbleArray));
            int[] resultBubble = BubbleSortAlgorithm.bubbleSort(bubbleArray);

            // Merge sort
            //System.out.println("Input array for MergeSort: " + Arrays.toString(mergeArray));
            MergeSortAlgorithm obM = new MergeSortAlgorithm();
            int[] resultMerge = obM.mergeSort(mergeArray, mergeArray.length);
            //System.out.println("Array after MergeSort: " + Arrays.toString(mergeArray));
            //System.out.println("Assignments MergeSort: " + resultMerge[0]);
            //System.out.println("Comparisons MergeSort: " + resultMerge[1]);
            //System.out.println("Comparisons MergeSort: " + resultMerge[2]);

            // Quick sort
            //System.out.println("Input array for QuickSort: " + Arrays.toString(quickArray));
            QuickSortAlgorithm obQ = new QuickSortAlgorithm();
            int[] resultQuick = obQ.quickSort(quickArray, 0, quickArray.length - 1);
            //System.out.println("Array after QuickSort: " + Arrays.toString(quickArray));
            //System.out.println("Assignments QuickSort: " + resultQuick[0]);
            //System.out.println("Comparisons QuickSort: " + resultQuick[1]);
            //System.out.println("Operations QuickSort: " + resultQuick[2]);

            try {
                Workbook wb = Workbook.getWorkbook(new File("Best case scenario.xls"));
                WritableWorkbook copy = Workbook.createWorkbook(new File("Best case scenario.xls"), wb);
                WritableSheet copySheet = copy.getSheet(0);
                Label label1 = new Label(1, numberArrays, String.valueOf(arrayDim));
                copySheet.addCell(label1);
                Label label2 = new Label(2, numberArrays, String.valueOf(resultBubble[0]));
                copySheet.addCell(label2);
                Label label3 = new Label(3, numberArrays, String.valueOf(resultBubble[1]));
                copySheet.addCell(label3);
                Label label4 = new Label(4, numberArrays, String.valueOf(resultBubble[2]));
                copySheet.addCell(label4);

                Label label5 = new Label(5, numberArrays, String.valueOf(resultMerge[0]));
                copySheet.addCell(label5);
                Label label6 = new Label(6, numberArrays, String.valueOf(resultMerge[1]));
                copySheet.addCell(label6);
                Label label7 = new Label(7, numberArrays, String.valueOf(resultMerge[2]));
                copySheet.addCell(label7);

                Label label8 = new Label(8, numberArrays, String.valueOf(resultQuick[0]));
                copySheet.addCell(label8);
                Label label9 = new Label(9, numberArrays, String.valueOf(resultQuick[1]));
                copySheet.addCell(label9);
                Label label10 = new Label(10, numberArrays, String.valueOf(resultQuick[2]));
                copySheet.addCell(label10);

                copy.write();
                copy.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}


